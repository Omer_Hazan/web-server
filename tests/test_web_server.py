import json
import uuid
from unittest.mock import call, AsyncMock
from uuid import UUID

import aioredis
import pytest
from fastapi import HTTPException

from web_server import web_server
from web_server.exc import MissingConnectionError
from web_server.rabbit_producer import RabbitProducer


@pytest.fixture()
def connections(monkeypatch):
    """
    Fixture for monkey patching web_server's app extra
    """
    redis_mock = AsyncMock(spec=aioredis.Redis)
    rabbit_mock = AsyncMock(spec=RabbitProducer)
    connections = dict(redis=redis_mock, rabbit=rabbit_mock)
    monkeypatch.setattr(web_server.app, 'extra', dict(connections=connections))
    return redis_mock, rabbit_mock


@pytest.mark.asyncio
async def test_calculate_expression_happyflow(monkeypatch, connections):
    """
    Happy flow test for calculate_expression - tests that publish is called
    twice and that the uid is valid
    """
    _, rabbit_mock = connections
    rabbit_mock.publish = AsyncMock()
    uid = await web_server.calculate_expression(1, 2, web_server.Operator.SUM)
    assert rabbit_mock.publish.call_count == 2
    assert str(UUID(uid.uid, version=4)) == uid.uid


@pytest.mark.asyncio
async def test_calculate_expression_publishes(mocker, connections):
    """
    Test that calculate_expression is calling publish with the expected args
    """
    uid = uuid.uuid4()
    _, rabbit_mock = connections
    rabbit_mock.publish = AsyncMock()
    mocker.patch.object(uuid, 'uuid4', return_value=uid)
    uid = await web_server.calculate_expression(1, 2, web_server.Operator.SUM)
    first_call = call(
        routing_key='calculate',
        msg=json.dumps(dict(a=1, b=2, operator=web_server.Operator.SUM.value,
                            uid=uid.uid)).encode())
    second_call = call(routing_key='remove_delay',
                       msg=json.dumps(dict(uid=uid.uid)).encode())
    rabbit_mock.publish.assert_has_calls([first_call, second_call])


@pytest.mark.asyncio
async def test_no_rabbit_connection():
    """
    Test that the expected error is raised when there is no connection
    to RabbitMQ
    """
    with pytest.raises(ConnectionError):
        await web_server.setup_rabbitmq('localhost:1234')


@pytest.mark.asyncio
async def test_no_redis_connection():
    """
    Test that the expected error is raised when there is no connection to Redis
    """
    with pytest.raises((ConnectionRefusedError, OSError), ):
        await web_server.setup_redis('localhost:1111/0')


@pytest.mark.asyncio
async def test_get_result(mocker, connections):
    """
    Test web_server.get_result happy flow functionality
    """
    uid, value = '1', b'hello world'
    patcher = mocker.patch('aioredis.Redis')
    redis_mock = patcher.return_value
    redis_mock.hget = AsyncMock(return_value=b'hello world')
    result = await web_server.get_result(uid)
    assert result == dict(result=value.decode())


@pytest.mark.asyncio
async def test_get_result_no_item(mocker, connections):
    """
    Test web_server.get_result when no such item functionality
    """
    uid, value = '1', b'hello world'
    patcher = mocker.patch('aioredis.Redis')
    redis_mock = patcher.return_value
    redis_mock.hget = AsyncMock(return_value=None)
    with pytest.raises(HTTPException):
        await web_server.get_result(uid)


@pytest.mark.asyncio
async def test_redis_setup_happy_flow(mocker):
    """
    Test that the global redis_pool is not None when redis_setup works
    """
    expected_result = AsyncMock()
    mocker.patch('aioredis.create_redis_pool', return_value=expected_result)
    mocker.patch('aioredis.Redis', return_value=AsyncMock())
    result = await web_server.setup_redis('localhost:1234/0')
    assert result == expected_result


@pytest.mark.asyncio
async def test_rabbit_setup_happy_flow(mocker):
    """
    Test that the global rabbit_prod is not None when rabbit_setup works
    """
    expected_result = AsyncMock()
    mocker.patch.object(web_server, 'RabbitProducer',
                        return_value=expected_result)
    result = await web_server.setup_rabbitmq('localhost:123')
    assert result == expected_result


@pytest.mark.asyncio
@pytest.mark.parametrize("pattern, exception", [('', AttributeError),
                                                ('localhost', AttributeError),
                                                ('1234', AttributeError)])
async def test_redis_invalid_uri_pattern(pattern, exception):
    """
    Tests the case when giving setup_redis invalid argument
    """
    with pytest.raises(exception):
        await web_server.setup_redis(pattern)


@pytest.mark.asyncio
@pytest.mark.parametrize("pattern, exception", [('', AttributeError),
                                                ('localhost', ValueError),
                                                ('1234', ValueError)])
async def test_rabbit_invalid_uri_pattern(pattern, exception):
    """
    Tests the case when giving setup_rabbitmq invalid argument
    """
    with pytest.raises(exception):
        await web_server.setup_rabbitmq(pattern)


@pytest.mark.asyncio
async def test_no_rabbit_connection_in_extra():
    """
    Test the case that there is no connections object in fastapi app's extra
    """
    with pytest.raises(MissingConnectionError):
        await web_server.calculate_expression(1, 2, web_server.Operator.SUM)


@pytest.mark.asyncio
async def test_no_redis_connection_in_extra():
    """
    Test the case that there is no connections object in fastapi app's extra
    """
    with pytest.raises(MissingConnectionError):
        await web_server.get_result('1')


@pytest.mark.asyncio
async def test_startup_event_happy_flow(mocker):
    """
    Test setup_event happy flow functionality
    """
    redis_conn_mock = mocker.Mock()
    rabbit_conn_mock = mocker.Mock()
    mocker.patch.object(web_server, 'setup_redis',
                        return_value=redis_conn_mock)
    mocker.patch.object(web_server, 'setup_rabbitmq',
                        return_value=rabbit_conn_mock)
    await web_server.startup_event()
    assert web_server.app.extra['connections']['redis'] == redis_conn_mock
    assert web_server.app.extra['connections']['rabbit'] == rabbit_conn_mock


@pytest.mark.asyncio
async def test_startup_event_exception_raised(mocker):
    """
    Test that startup_event raises an exception if one is raised
    """
    mocker.patch.object(web_server, 'setup_redis',
                        side_effect=AttributeError())
    mocker.patch.object(web_server, 'setup_rabbitmq')
    with pytest.raises(AttributeError):
        await web_server.startup_event()


@pytest.mark.asyncio
async def test_shutdown_event_happy_flow(connections):
    """
    Test shutdown_event happy flow functionality
    """
    redis_close_mock = AsyncMock()
    rabbit_close_mock = AsyncMock()
    web_server.app.extra["connections"]["redis"].close = redis_close_mock
    web_server.app.extra["connections"]["rabbit"].close = rabbit_close_mock
    await web_server.shutdown_event()
    redis_close_mock.assert_called_once()
    rabbit_close_mock.assert_called_once()

