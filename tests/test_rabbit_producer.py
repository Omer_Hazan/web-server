from unittest.mock import AsyncMock

import aio_pika
import pytest

from web_server.exc import NoConnectionError
from web_server.rabbit_producer import RabbitProducer


@pytest.fixture()
def rabbit_producer(mocker):
    """
    Fixture for RabbitProducer which mocks connections
    """
    rabbit_prod = RabbitProducer('1.1.1.1', 1111)
    rabbit_prod._conn = mocker.Mock()
    rabbit_prod._channel = mocker.Mock()
    return rabbit_prod


@pytest.mark.asyncio
@pytest.mark.parametrize("conn, is_closed, should_connect",
                         [(True, True, True),
                          (True, False, False),
                          (False, True, True)])
async def test_connect(mocker, rabbit_producer, conn, is_closed,
                       should_connect):
    """
    Test RabbitProducer.connect method with different self._conn
    and self_conn.is_closed cases
    """
    if conn:
        rabbit_producer._conn.is_closed = is_closed
        rabbit_producer._channel.is_closed = is_closed
    mocked_blocking_connection = mocker.patch('aio_pika.connect_robust')
    await rabbit_producer.connect()
    if should_connect:
        mocked_blocking_connection.assert_called_once()
    else:
        mocked_blocking_connection.assert_not_called()


@pytest.mark.asyncio
async def test_close(rabbit_producer):
    """
    Test RabbitProducer.test_close functionality
    """
    rabbit_producer._conn.is_closed = False
    close_mock = AsyncMock()
    rabbit_producer._conn.close = close_mock
    await rabbit_producer.close()
    close_mock.assert_called_once()


@pytest.mark.asyncio
async def test_publish_happy_flow(rabbit_producer):
    """
    Test RabbitProducer.publish functionality
    """
    mocked_publish = AsyncMock()
    rabbit_producer._channel.default_exchange.publish = mocked_publish
    await rabbit_producer.publish('test', b'hello')
    mocked_publish.assert_called_once()


@pytest.mark.asyncio
async def test_publish_failure(rabbit_producer):
    """
    Test RabbitProducer.publish in case of connection failure
    """
    mocked_publish = AsyncMock(
        side_effect=aio_pika.exceptions.AMQPConnectionError())
    rabbit_producer._channel.default_exchange.publish = mocked_publish
    mocked_connect = AsyncMock()
    rabbit_producer.connect = mocked_connect
    with pytest.raises(aio_pika.exceptions.AMQPConnectionError):
        await rabbit_producer.publish('test', b'hello')
    assert mocked_publish.call_count == 2
    mocked_connect.assert_called_once()


@pytest.mark.asyncio
async def test_publish_no_connection(rabbit_producer):
    """
    Test RabbitProducer.publish when RabbitProducer._conn is None
    """
    rabbit_producer._conn = None
    with pytest.raises(NoConnectionError):
        await rabbit_producer.publish('test', b'hello')
