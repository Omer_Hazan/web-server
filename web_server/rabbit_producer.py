from typing import Optional

import aio_pika
from aio_pika import RobustConnection, RobustChannel

from web_server.exc import NoConnectionError


class RabbitProducer:
    """
    Contains functionality for connecting and publishing messages for RabbitMQ

    Args:
        host: The RabbitMQ's host
        port: The RabbitMQ's port

    Attributes:
        _host: The RabbitMQ's host
        _port: The RabbitMQ's port
        _conn (aio_pika.robust_connection.RobustConnection): The connection to
            the RabbitMQ
        _channel (aio_pika.robust_channel.RobustChannel): The channel created
            for publishing messages in the RabbitMQ

    """

    def __init__(self, host: str, port: int):
        self._host = host
        self._port = port
        self._conn: Optional[RobustConnection] = None
        self._channel: Optional[RobustChannel] = None

    async def connect(self):
        """
        Connecting to the RabbitMQ in case there is no open connection
        """
        if not self._conn or self._conn.is_closed or self._channel.is_closed:
            self._conn = await aio_pika.connect_robust(
                host=self._host, port=self._port
            )
            self._channel = await self._conn.channel()

    async def close(self):
        """
        Closing the connection to the RabbitMQ
        """
        if self._conn and not self._conn.is_closed:
            await self._conn.close()

    async def _publish(self, routing_key: str, msg: bytes):
        """
        Publish given message in the queue

        Args:
            routing_key: The message's routing key
            msg: The message's contents

        """
        if self._conn is None or self._channel is None:
            raise NoConnectionError
        await self._channel.default_exchange.publish(  # type: ignore
            aio_pika.Message(msg), routing_key=routing_key,
        )

    async def publish(self, routing_key: str, msg: bytes):
        """
        Publish given message in the queue, in case of connection
        failure - reconnect to the RabbitMQ

        Args:
            routing_key: The message's routing key
            msg: The message's contents

        """
        try:
            await self._publish(routing_key, msg)
        except aio_pika.exceptions.CONNECTION_EXCEPTIONS:
            await self.connect()
            await self._publish(routing_key, msg)
