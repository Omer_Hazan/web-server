import json
import os
import re
import sys
import uuid
from enum import Enum

import aioredis
import logbook
from fastapi import FastAPI, HTTPException
from pydantic import BaseModel

from web_server.conf import REDIS_URI_PATTERN, RABBIT_HOST_PATTERN, REDIS_KEY
from .exc import MissingConnectionError
from .rabbit_producer import RabbitProducer

# FastAPI app
app = FastAPI()

# Setup logging
logbook.StreamHandler(sys.stdout).push_application()
logger = logbook.Logger("web_server")


class Operator(Enum):
    """
    Mathematical operator options
    """

    SUM = "sum"
    SUB = "sub"
    MUL = "mul"
    DIV = "div"


async def setup_redis(redis_uri: str):
    """
    Creates a Redis connection pool and test the connection to it

    Args:
        redis_uri: The redis's uri

    Raises:
        AttributeError: If the regex pattern is not found in the given URI

    """
    redis_m = re.search(REDIS_URI_PATTERN, redis_uri)
    if redis_m is None:
        raise AttributeError("Redis URI is invalid")
    address = f"redis://{redis_m.group('address')}"
    redis_pool = await aioredis.create_redis_pool(
        address=address, db=int(redis_m.group("db")),
    )
    conn_test = aioredis.Redis(pool_or_conn=redis_pool)
    await conn_test.ping()
    logger.debug("Successfully connected to Redis")
    return redis_pool


async def setup_rabbitmq(rabbit_host: str):
    """
    Setup the connection for RabbitMQ

    Args:
        rabbit_host: The RabbitMQ's host to connect to

    Raises:
        AttributeError: If the regex pattern is not found in the given URI

    """
    rabbit_m = re.search(RABBIT_HOST_PATTERN, rabbit_host)
    if rabbit_m is None:
        raise AttributeError("Rabbit host is invalid")
    rabbit_prod = RabbitProducer(
        rabbit_m.group("host"), int(rabbit_m.group("port"))
    )
    await rabbit_prod.connect()
    logger.debug("Successfully connected to RabbitMQ")
    return rabbit_prod


class UIDResponse(BaseModel):
    """
    Model represents UID response for `calculate_expression`
    """

    uid: str


@app.get("/calc/", response_model=UIDResponse)
async def calculate_expression(
    a: float, b: float, operator: Operator,
) -> UIDResponse:
    """
    Calculate expression API
        1. Sets an UID for the request
        2. Sends the relevant data to the relevant queues

    Args:
        a: The first operand
        b: The second operand
        operator: The operator for the expression

    Raises:
        MissingConnectionError: If rabbit connection object could not be
        found in fastapi app's extra

    Returns: The uid generated for the request.

    """
    uid = str(uuid.uuid4())
    logger.debug(f"Calculate expression: {a}, {b}, {operator} - {uid}")
    if (
        "connections" not in app.extra
        or "rabbit" not in app.extra["connections"]
        or not isinstance(app.extra["connections"]["rabbit"], RabbitProducer)
    ):
        raise MissingConnectionError("Could not find connection to rabbit obj")
    rabbit_conn = app.extra["connections"]["rabbit"]
    await rabbit_conn.publish(
        routing_key="calculate",
        msg=json.dumps(
            dict(a=a, b=b, operator=operator.value, uid=uid)
        ).encode(),
    )
    await rabbit_conn.publish(
        routing_key="remove_delay", msg=json.dumps(dict(uid=uid)).encode()
    )
    return UIDResponse(uid=uid)


class ResultResponse(BaseModel):
    """
    Model represents result response for `get_result`
    """

    result: str


@app.get("/result/{uid}", response_model=ResultResponse)
async def get_result(uid: str) -> ResultResponse:
    """
    Get result of an uid API

    Args:
        uid: The uid to retrieve the value for.

    Raises:
        MissingConnectionError: If redis connection object could not be
        found in fastapi app's extra

    Returns: The value for the uid

    """
    if (
        "connections" not in app.extra
        or "redis" not in app.extra["connections"]
    ):
        raise MissingConnectionError("Could not find connection to redis obj")
    redis_conn = aioredis.Redis(pool_or_conn=app.extra["connections"]["redis"])
    val = await redis_conn.hget(REDIS_KEY, str(uid))
    if val is None:
        raise HTTPException(status_code=404, detail="Item not found")
    val = val.decode()
    return ResultResponse(result=val)


@app.on_event("startup")
async def startup_event():
    """
    Startup event when uvicorn starts this app.
    Initialize connections to Redis and Rabbitmq
    """
    redis_uri = os.environ.get("REDIS", "localhost:6379/0")
    rabbit_host = os.environ.get("RABBIT", "localhost:5672")
    try:
        app.extra["connections"] = dict()
        app.extra["connections"]["redis"] = await setup_redis(redis_uri)
        app.extra["connections"]["rabbit"] = await setup_rabbitmq(rabbit_host)
    except Exception:
        logger.exception("Failed to startup web server")
        raise


@app.on_event("shutdown")
async def shutdown_event():
    """
    Startup event when app is being closed.
    Close connections to Redis and Rabbitmq
    """
    if "connections" in app.extra and "redis" in app.extra["connections"]:
        await app.extra["connections"]["redis"].close()
    if "connections" in app.extra and "rabbit" in app.extra["connections"]:
        await app.extra["connections"]["rabbit"].close()
