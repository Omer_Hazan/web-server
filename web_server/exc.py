class MissingConnectionError(Exception):
    """
    Exception that used when the connection object is not found in fastapi
    app's extra
    """


class NoConnectionError(Exception):
    """
    Exception that used when trying to publish a message with
    RabbitProducer with no connection
    """
