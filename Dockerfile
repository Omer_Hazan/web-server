FROM       tiangolo/uvicorn-gunicorn-fastapi:python3.8
RUN        pip install poetry

WORKDIR    /app
COPY       poetry.lock pyproject.toml /app/

RUN        poetry config virtualenvs.create false --no-interaction --no-ansi
RUN        poetry install --no-root

COPY       . /app
CMD        ["gunicorn", "web_server.web_server:app", "-k", "uvicorn.workers.UvicornWorker", "--bind", "0.0.0.0:8000"]
